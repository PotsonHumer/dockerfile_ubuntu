FROM ubuntu:16.04

MAINTAINER potsonhumer@gmail.com

# For add ssh key
ADD ./ssh /root/.ssh/
RUN chmod -R 600 ~/.ssh/


# For Basic setup
RUN apt-get clean && apt-get update && apt-get install -y locales
RUN apt-get install -y vim && apt-get install -y git && apt-get install -y curl


# For GIT LFS
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
RUN apt-get install -y git-lfs


# For Setup git profile
RUN git config --global user.name "PotsonHumer"
RUN git config --global user.email "potsonhumer@gmail.com"


# For install Node.js
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash
RUN apt-get install -y nodejs


#For install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn


# For zh_TW
RUN locale-gen zh_TW.UTF-8
ENV LANG zh_TW.UTF-8
ENV LANGUAGE zh_TW.UTF-8
ENV LC_ALL zh_TW.UTF-8


# For bash setup
RUN echo "PS1='\[\033[1;36m\]\u\[\033[1;31m\]@\[\033[1;32m\]\h:\[\033[1;35m\]\w\[\033[1;31m\]\$\[\033[0m\]'" >> ~/.bashrc
RUN echo "LS_COLORS=$LS_COLORS:'di=0;95:' ; export LS_COLORS;" >> ~/.bashrc
RUN echo "source /usr/share/bash-completion/completions/git" >> ~/.bashrc


# For vim color theme
RUN echo "colorscheme blue" >> ~/.vimrc